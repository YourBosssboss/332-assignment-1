package week5work;

public class Stuff {
	public static final int MATCH   = 1;
	public static final int REPLACE = 2;
	public static final int INSERT  = 2;
	public static final int DELETE  = 2;
	public static final int TWIDDLE = 2;
	
	public static int editDistance(String a, String b) {
		int lenA = a.length();
		int lenB = b.length();
		
		int[][] table = new int[a.length()+1][b.length()+1];

		for(int i = lenA; i >= 0; i--) {
			for(int j = lenB; j >= 0; j--) {
				if(i == lenA && j == lenB) continue;
				int m = Integer.MAX_VALUE;
				
				if(i < lenA && j < lenB) {
					int cost = a.charAt(i) == b.charAt(j) ? MATCH : REPLACE;		
					m = Math.min(m, cost + table[i + 1][j + 1]);
				}
									
				if(i < lenA) m = Math.min(m, DELETE + table[i + 1][j]);
				if(j < lenB) m = Math.min(m, INSERT + table[i][j + 1]);
				
				table[i][j] = m;
			}
		}
				
		return table[0][0];
		
	}
	
	public static void main(String[] args){
		
		int cost = editDistance("algorithm", "altruistic");
		System.out.println(cost);
	}

}
